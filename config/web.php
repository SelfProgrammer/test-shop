<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'test-shop',
    'name' => 'Test Shop',
    'basePath' => dirname(__DIR__),
    
    'language' => 'ru-RU',
    'bootstrap' => ['log', 'accounts', 'admin', 'shop', 'app'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads/images',
            'uploadUrl' => '@web/uploads/images',
            'imageAllowExtensions' => ['jpg','png','gif']
        ],
        'accounts' => [
            'class' => 'app\modules\accounts\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'shop' => [
            'class' => 'app\modules\shop\Module',
        ],
        'app' => [
            'class' => 'app\modules\app\Module',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'vuB8P4wz2hJHMj54w7ypXFCynG3O6J7v',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => '\app\modules\accounts\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/accounts/user/signin'],
        ],
        'authManager' => require(__DIR__ . '/auth-manager.php'),
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'login' => 'accounts/user/signin',
                'contact' => 'site/contact',
                'about' => 'site/about',
                '' => 'site/index',
                '/' => 'site/index',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
