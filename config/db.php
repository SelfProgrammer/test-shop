<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=test_shop',
    'username' => 'root',
    'password' => '',
    // 'dsn' => 'mysql:host=localhost;dbname=id6822514_shop',
    // 'username' => 'id6822514_shop',
    // 'password' => 'shop1221',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
