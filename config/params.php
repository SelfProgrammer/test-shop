<?php

return [
    'adminEmail' => 'admin@example.com',
    'editor_plugins_list_full' => [
        'fontcolor',
        'fontfamily',
        'fontsize',
        'imagemanager',
        'filemanager',
        'table',
        'video',
        'source',
        'alignment',
        'table',
        'clips',
    ],
    'image_cashe_sizes' => [
        'mini' => [110, 110],
        'small' => [250, 250],
        'normal' => [450, 450],
    ],
];
