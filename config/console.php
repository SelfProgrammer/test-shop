<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'accounts', 'admin', 'shop', 'app'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'accounts' => [
            'class' => 'app\modules\accounts\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'shop' => [
            'class' => 'app\modules\shop\Module',
        ],
        'app' => [
            'class' => 'app\modules\app\Module',
        ],
    ],
    'components' => [
        'authManager' => require(__DIR__ . '/auth-manager.php'),
        'session' => [ // for use session in console application
            'class' => 'yii\web\Session'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
