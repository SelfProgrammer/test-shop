<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="col-md-3">
    <div class="prod-card">
        <div class="prod-img-wrap">
            <?= Html::img($model->getImage('small'))?>
        </div>
        <div class="prod-title">
            <?= $model->name?>
        </div>
        <div class="prod-price">
            <?php if ($model->special_price) :?>
                <span><?= $model->special_price ?> грн.</span>
            <?php endif;?>
            <span><?= $model->price ?> грн.</span>
        </div>
        <div class="btn-wrap">
            <a href="<?= Url::to(['/shop/product/view', 'slug' => $model->slug, 'id' => $model->id]) ?>" class="btn btn-default">Подробнее</a>
        </div>
    </div>
</div>