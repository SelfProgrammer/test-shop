<?php

/* @var $this yii\web\View */

?>
<?= $this->render('_slideshow')?>
<div class="site-index">

    <div class="body-content">
        
        <?php foreach ($products as $product) :?>
            <?= $this->render('_product_item', [
                'model' => $product,
            ])?>
        <?php endforeach;?>

    </div>
</div>
