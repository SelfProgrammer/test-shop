<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\BreadcrumbsUtility;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<header class="header">
    <div class="container">
        <div class="col-md-3 logo">
            <a href="/">
                <img src="/uploads/logo.png" />
            </a>
        </div>
        <div class="col-md-9">
            <div class="top-search">
                <form action="<?= \yii\helpers\Url::to(['/shop/product/search'])?>">
                    <div class="input-group"> 
                        <input class="form-control" placeholder="искать..." name="q"> 
                        <span class="input-group-btn"> 
                            <button class="btn btn-default btn-top-search" type="submit">Поиск</button> 
                        </span> 
                    </div>
                </form>
            </div>
            <p class="slogan">На крайний случай в них можно спать ;)</p>
        </div>
    </div>
</header>
<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse navbar navbar-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => \app\modules\shop\models\Category::getFrontTopMenu(),
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => BreadcrumbsUtility::getHome('Главная', Yii::$app->getHomeUrl()),
            'links' => isset($this->params['breadcrumbs']) ? BreadcrumbsUtility::UseMicroData($this->params['breadcrumbs']) : [],
            'options' => [
                'class' => 'breadcrumb',
                'itemscope itemtype' => 'https://schema.org/BreadcrumbList'
            ],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    <?php if (isset(\Yii::$app->params['metaText']) && !empty(\Yii::$app->params['metaText'])) : ?>
        <div class="container bottom-text">
            <?= \Yii::$app->params['metaText']?>
        </div>
    <?php endif; ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
