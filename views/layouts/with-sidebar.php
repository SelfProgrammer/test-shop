<?php $this->beginContent('@app/views/layouts/main.php') ?>
<div class="col-md-3">
    <?= isset($this->params['sidebarContent']) ? $this->params['sidebarContent'] : '' ?>
</div>
<div class="col-md-9">
<?= $content ?>    
</div>
<?php $this->endContent() ?>