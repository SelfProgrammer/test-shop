<?php

namespace app\modules\admin;

use Yii;
use yii\helpers\Url;
use yii\web\ErrorHandler;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $layout = 'main';

    private $_navLeft  = [];
    private $_navRight = [];
    private $_navGroups = [
        0 => [
            'label' => 'General',
            'items' => [],
            'sort'  => 9999,
        ]
    ];
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
        Yii::setAlias('@admin', '@app/modules/admin');
        if (Yii::$app instanceof \yii\web\Application) {
        }
        $this->urlManagerRulesInit();
    }
    
    public function bootstrap($app)
    {
        //var_dump(Yii::$app->controller);
        // TODO: Implement bootstrap() method.
    }


    public function addLinkToLeftMenuItem($key, $item)
    {
        if (!property_exists($this, '_navLeft')) {
            return false;
        }
        if (!is_array($item)) {
            return false;
        }
        if (!isset($this->_navLeft[$key])) {
            return false;
        }
        $this->_navLeft[$key]['items'][] = $item;
        
        return true;
    }
    
    public function addLeftMenuItem($item, $key = false)
    {
        return $this->addMenuItem('_navLeft', $item, $key);
    }

    public function getLeftMenuItems()
    {
        return $this->getMenuItems('_navLeft');
    }

    public function addRightMenuItem($item)
    {
        return $this->addMenuItem('_navRight', $item);
    }

    public function getRightMenuItems()
    {
        return $this->getMenuItems('_navRight');
    }

    /**
     * Добавляет новый пункт меню в навигацию админ панели
     * Если добавить в массив ключ sort то можно задать сортировку (DESC)
     * Пример:
     *    $item = [
     *        'label' => 'Home',
     *        'url' => ['site/index'],
     *        'linkOptions' => [...],
     *        'sort' => 3,
     *        'controllerId' => 'items', //id контроллера для того что бы подсвечивать активным пункт меню для всего контроллера, а не только для отдельного action (например для crud)
     *    ];
     * @param $item
     * @return bool
     */
    private function addMenuItem($attr, $item, $key)
    {
        if (!property_exists($this, $attr)) {
            return false;
        }
        if (!is_array($item)) {
            return false;
        }
//        array_push(, $item);
        if ($key) {
            $this->{$attr}[$key] = $item;
        } else {
            $this->{$attr}[] = $item;
        }
        return true;
    }

    private function getMenuItems($attr)
    {
        if (!property_exists($this, $attr)) {
            return false;
        }
        $sort = [];
        //var_dump(Yii::$app->controller->action->id);
        $nav = array_map(function($item){
            if (isset($item['controllerId'])) {
                $active = false;
                if (is_string($item['controllerId']) && Yii::$app->controller->id === $item['controllerId']) {
                    $active = true;
                }
                if (is_array($item['controllerId']) && in_array(Yii::$app->controller->id, $item['controllerId'])) {
                    $active = true;
                }
                if (isset($item['exceptionActions'])) {
                    if (is_string($item['exceptionActions']) && $item['exceptionActions'] === Yii::$app->controller->action->id) {
                        $active = false;
                    }
                    if (is_array($item['exceptionActions']) && in_array(Yii::$app->controller->action->id, $item['exceptionActions'])) {
                        $active = false;
                    }
                }
                if ($active) {
                    $item['submenuTemplate'] = '<ul class="nav child_menu" style="display: block;">{items}</ul>';
                }
                $item['active'] = $active;
            }
            //var_dump(Yii::$app->controller->id);
            if (isset($item['items']) && is_array($item['items'])) {
                foreach ($item['items'] as $subKey => $subItem) {

                    if (isset($subItem['controllerId'])) {
                        $subActive = false;
                        if (is_string($subItem['controllerId']) && Yii::$app->controller->id === $subItem['controllerId']) {
                            $subActive = true;
                        }
                        if (is_array($subItem['controllerId']) && in_array(Yii::$app->controller->id, $subItem['controllerId'])) {
                            $subActive = true;
                        }
                        if (isset($subItem['exceptionActions'])) {
                            if (is_string($subItem['exceptionActions']) && $subItem['exceptionActions'] === Yii::$app->controller->action->id) {
                                $subActive = false;
                            }
                            if (is_array($subItem['exceptionActions']) && in_array(Yii::$app->controller->action->id, $subItem['exceptionActions'])) {
                                $subActive = false;
                            }
                        }
                        $item['items'][$subKey]['active'] = $subActive;
                    }
                }
            }
            return $item;
        },$this->{$attr});
        foreach ($nav as $key => $item) {
            $sort[$key] = 0;
            if (isset($item['sort'])) {
                $sort[$key] = $item['sort'];
            }
        }
        array_multisort($sort, SORT_DESC, $nav);
        return $nav;
    }


    /**
     * Добавляет контроллеры в модуль
     * Используется для того что бы другие модули могли добалять свои контроллеры для управления своим модулем
     *
     * @param $controllerName
     * @param $config
     *
     * @return bool
     */
    public function addControllerMap($controllerName, $config)
    {
        if (isset($this->controllerMap[$controllerName])) {
            return false;
        }
        $this->controllerMap[$controllerName] = $config;
        return true;
    }


    /**
     * @param string $label заголовок группы
     * @param array $items элементы меню для \app\modules\admin\widgets\Menu
     * @param null|integer $sort позиция группы среди всех групп, если null то будет назначено по умолчанию
     */
    public function createNavGroup($label, $items, $sort = null)
    {
        $this->_navGroups[] = [
            'label' => $label,
            'sort'  => $sort,
            'items' => $items,
        ];
    }

    /**
     * Возвращает массив групп
     * @return array
     */
    public function getNavGroups()
    {
        $groups = $this->_navGroups;
        $defaultSortValue = count($groups);
        foreach ($groups as $key => $group) {
            if (isset($group['sort'])) {
                $sort[$key] = $group['sort'];
            } else {
                $sort[$key] = $defaultSortValue;
                $defaultSortValue--;
            }
        }
        array_multisort($sort, SORT_DESC, $groups);
        return $groups;
    }

    protected function groupItemsProcessing($items)
    {
        return array_map(function($item){
            
        }, $items);
    }

    /**
     * Добавляет пункт меню в основную группу меню
     * @param array $item
     */
    public function addItemToGeneralNavGroup($item)
    {
        $this->_navGroups[0]['items'][] = $item;
    }

    public static function homeRoute()
    {
        return ['/admin/default/index'];
    }

    public static function homeUrl()
    {
        return Url::to(self::homeRoute());
    }


    /**
     * Добавляет роуты
     */
    public function urlManagerRulesInit()
    {
        //use \yii\web\UrlRule;
        $rules = [
            'admin/<controller>/<action>' => 'admin/<controller>/<action>',
            'admin/<controller>' => 'admin/<controller>/index',
            'admin' => 'admin/default/index',
        ];

        Yii::$app->urlManager->addRules($rules);
    }
}
