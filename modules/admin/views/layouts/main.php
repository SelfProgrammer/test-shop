<?php

use yii\widgets\Breadcrumbs;
use app\modules\admin\widgets\Menu as MainMenuGroup;
use app\modules\admin\Module;

/* @var $this \yii\web\View */
/* @var $content string */


?>
<?php $this->beginContent('@admin/views/layouts/layout.php') ?>
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?= Module::homeUrl() ?>" class="site_title"><i class="fa fa-gears"></i> <span>Test Shop</span></a>
                    <a href="/" target="_blank" class="site_title2">перейти на сайт</a>
                </div>

                <div class="clearfix"></div>

        

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <?= MainMenuGroup::widget([
                            'options' => ['class' => 'nav side-menu'],
                            'items' => Module::getInstance()->getLeftMenuItems(),
                            'encodeLabels' => false,
                            'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
                            'encodeLabels' => false, //allows you to use html in labels
                            'activateParents' => true,
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <?php if (($user = Yii::$app->getUser()->getIdentity()) !== null): ?>
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<?= $user->getAvatar() ?>" alt=""><?= $user->name ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="<?= \yii\helpers\Url::to(['/accounts/user/signout']) ?>"><i class="fa fa-sign-out pull-right"></i> Выход</a></li>
                            </ul>
                        </li>
                        <?php endif; ?>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'homeLink' => [
                    'label' => 'Админ-панель',
                    'url' => ['/admin/default/index'],
                ],
                'options' => [
                    'class' => 'breadcrumb',
                    'style' => 'margin-left: 0;background: #ededed;margin-top: 70px;',
                ]
            ]) ?>
            <?= $content ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <!--<div class="pull-right">-->
            <!--    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>-->
            <!--</div>-->
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<?php
//$preloaderIcon = $theme->getPreloader();
$preloaderIcon = '';
$this->registerJs(<<<JS

$(document).on('pjax:send', function(e,options) {
  var container = $(e.target).closest('.preloader-container');
  if (container.length > 0) {
    container.append('<div class="preloader-overlay"></div>');
  }
  console.log('pjax:send');
});
$(document).on('pjax:complete', function() {
  $('.preloader-overlay').fadeOut(500, function(){
    $(this).remove();
  });
  console.log('pjax:complete');
});
JS
    , \yii\web\View::POS_READY, 'pjax_preloader');

$this->registerCss(<<<CSS
.preloader-container {
    position: relative;
}
.preloader-overlay {
    background-color: rgba(255, 255, 255, 0.75);
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    /*background-image: url({$preloaderIcon});*/
    background-repeat: no-repeat;
    background-position: center center;
    background-attachment: fixed;
    z-index: 9999;
}
.nav.child_menu>li>a .fa.fa-chevron-down {
    position: absolute;
    right: 0;
    top: 6px;
}
.site_title2 {
    color: #fff;
    margin-left: 12px;
}
.nav_title {
    height: 80px;
}
CSS
    , [], 'pjax_preloader_styles')

?>
<?php $this->endContent() ?>