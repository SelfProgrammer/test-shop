<?php

Yii::$app->controller->layout = 'layout';

?>
<div class="container body">
    <div class="main_container">
        <!-- page content -->
        <div class="col-md-offset-4 col-md-4">
            <div class="col-middle">

                <div class="text-center text-center">
                    <h1 class="error-number">...</h1>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <h2>Выгружаю дополнительно обновление</h2>
                        <p>Скоро все заработает :)</p>
                    </div>

                    <!--<div class="mid_center">-->
                    <!--    <h3>Search</h3>-->
                    <!--    <form>-->
                    <!--        <div class="col-xs-12 form-group pull-right top_search">-->
                    <!--            <div class="input-group">-->
                    <!--                <input type="text" class="form-control" placeholder="Search for...">-->
                    <!--  <span class="input-group-btn">-->
                    <!--          <button class="btn btn-default" type="button">Go!</button>-->
                    <!--      </span>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </form>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>