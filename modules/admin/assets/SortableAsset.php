<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;


class SortableAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/vendor/bower/Sortable';
    public $js = [
        'Sortable.min.js',
        'jquery.binding.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
