<?php


namespace app\modules\admin\assets;

use yii\web\AssetBundle;


class GentelellaBootstrapThemeAsset extends AssetBundle
{
    public $sourcePath = '@admin/vendor/bower/gentelella/build';
    public $css = [
        'css/custom.min.css',
    ];
    public $js = [
        'js/custom.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\modules\admin\assets\FontAwesomeAsset',
    ];
}
