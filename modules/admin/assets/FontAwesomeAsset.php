<?php


namespace app\modules\admin\assets;

use yii\web\AssetBundle;


class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@admin/vendor/bower/font-awesome';
    public $css = [
        'css/font-awesome.min.css',
    ];

    public $depends = [

    ];
}
