<?php

namespace app\modules\admin\assets;


class BootstrapDialogAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@admin/vendor/bower/bootstrap3-dialog/dist';
    public $css = [
        'css/bootstrap-dialog.min.css',
    ];

    public $js = [
        'js/bootstrap-dialog.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
