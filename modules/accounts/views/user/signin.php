<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\accounts\models\SigninForm */
/* @var $form ActiveForm */

Yii::$app->controller->layout = 'login';

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;

//$mainTheme = ThemeAsset::register(new \yii\web\View());

?>


<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="x_panel">
            <div class="x_title" style="border-bottom: 0;">
                <h1>Test Shop</h1>
            </div>
            <div class="x_content">
                <br>
                <!--<form id="demo-form2" data-parsley-validate="" class="form-vertical form-label-left" novalidate="">-->

                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'login') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    <div class="form-group">
                        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary pull-right']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                <!--</form>-->
            </div>
        </div>
    </div>
</div>