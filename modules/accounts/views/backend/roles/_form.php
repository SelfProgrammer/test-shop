<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\accounts\models\backend\RolesSettings;

/* @var $this yii\web\View */
/* @var $model RolesSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_role') ?>

    <?php foreach ($model->settingsDefault as $key_module => $module):?>
        <?php foreach ($module as $key_block => $block):?>
            <?php app\modules\admin\widgets\Panel::begin()?>
            <h3><?= $block['block_name']?></h3>
            <?php foreach ($block['controllers'] as $controller_key => $controller):?>
                <?php foreach ($controller as $key => $controllerItem):?>
                    <?= $form->field($model, 'settingsArr['.$key_module.']['.$key_block.'][controllers]['.$controller_key.']['.$key.'][allowed]')
                        ->checkbox([
                            'label' => $controllerItem['name'],
                        ]) 
                    ?>
                <?php endforeach;?>
            <?php endforeach;?>
            <?php app\modules\admin\widgets\Panel::end()?>
        <?php endforeach;?>
    <?php endforeach;?>
    
    
    
    <div class="form-group">
        <?= Html::submitButton('Сохранить', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
