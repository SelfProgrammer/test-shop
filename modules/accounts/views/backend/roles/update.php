<?php

use yii\helpers\Html;
use app\modules\accounts\Module;

/* @var $this yii\web\View */
/* @var $model app\modules\accounts\models\User */

$this->title = 'Редактирование роли доступа';
$this->params['breadcrumbs'][] = ['label' => 'Роли доступа', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
