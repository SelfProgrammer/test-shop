<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\accounts\models\backend\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
        
$this->title = 'Аккаунты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'avatar',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\modules\accounts\models\User $model */
                    return Html::img($model->getAvatar(),[
                        'class' => 'img-circle',
                    ]);
                },
            ],
            'name',
            'email',
//            'position',
//            [
//                'attribute'=>'position',
//                'filter' => \app\modules\accounts\models\backend\RolesSettings::getRolesDropdownList(),
//                'value' => function ($model) {
//                    return $model->getRoleByPosition($model->position);
//                },
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
