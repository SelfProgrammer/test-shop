<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\accounts\models\backend\RolesSettings;

/* @var $this yii\web\View */
/* @var $model app\modules\accounts\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data'
        ],
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'position')->dropDownList(RolesSettings::getRolesDropdownList(), ['prompt' => '']) ?>


    <?= $form->field($model, 'avatar_upload')->fileInput() ?>
    <?php if ($avatar = $model->getAvatar()): ?>
        <?= Html::img($avatar) ?>
        <?= $form->field($model, 'avatar_delete')->checkbox() ?>
    <?php endif ?>
    

    <div class="form-group">
        <?= Html::submitButton('Сохранить', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
