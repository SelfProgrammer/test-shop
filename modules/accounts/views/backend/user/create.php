<?php

use yii\helpers\Html;
use app\modules\accounts\Module;


/* @var $this yii\web\View */
/* @var $model app\modules\accounts\models\User */

$this->title = 'Добавление пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Аккаунты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
