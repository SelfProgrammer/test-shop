<?php

namespace app\modules\accounts\controllers;

use Yii;
use app\modules\accounts\models\SigninForm;

class UserController extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['signin'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['signout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionSignin()
    {
        $model = new SigninForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/admin/default/index']);
        }
        return $this->render('signin', [
            'model' => $model,
        ]);
    }

    public function actionSignout()
    {
        Yii::$app->getUser()->logout(true);
        return $this->redirect(['/login']);
    }
}
