<?php

namespace app\modules\accounts\models\backend;

use Yii;
use app\modules\accounts\Module;

/**
 * This is the model class for collection "accounts_roles_settings".
 *
 * @property $id
 * @property mixed $user_role
 * @property mixed $settings
 */
class RolesSettings extends \yii\db\ActiveRecord
{    
    /**
     * string Сценарий для добавления 
     */
    const SCENARIO_CREATE = 'create';

    /**
     * string Сценарий для обновления информации
     */
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accounts_roles';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'id',
            'user_role',
            'settings',
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['user_role', 'settings', 'settingsArr'],
            self::SCENARIO_UPDATE => ['user_role', 'settings', 'settingsArr'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_role'], 'required'],
            [['user_role', 'settings', 'settingsArr'], 'safe']
        ];
    }

    public $settingsDefault;
    
    public function __construct($config = array()) 
    {
        $this->settingsDefault = $this->loadBasicSettings();
        return parent::__construct($config);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'user_role' => 'Название',
            'settings'  => 'Настройки',
        ];
    }
    
    public function beforeDelete() 
    {
        $role = Yii::$app->authManager->getRole((string)$this->_id);
        if ($role) {
            Yii::$app->authManager->remove($role);
        }
        return parent::beforeDelete();
    }
    
    public static function getRolesDropdownList()
    {
        $models = RolesSettings::find()->all();
        $outputArr = [];
        foreach ($models as $model) {
            if ($model->user_role == 'admin') {
                $outputArr[$model->id] =  'Администратор'; continue;
            }
            $outputArr[$model->id] = $model->user_role;
        }
        return $outputArr;
    }
    
    public static function onlyAdmin()
    {
//        return ['@'];
        $model = RolesSettings::find()->where(['user_role' => 'admin'])->one();
        if (!$model) {
            return ['nothingFoundForOnlyAdmin'];
        }
        return [$model->id];
    }
    
    public static function getRoleLabel($id)
    {
        $model = RolesSettings::findOne($id);
        if (!$model) {
            return null;
        }
        return $model->user_role;
    }

    public $settingsArr = [];
    
    public function afterFind() 
    {
        $this->settingsArr = \yii\helpers\Json::decode($this->settings);
        return parent::afterFind();
    }
    
    public function beforeSave($insert) 
    {
        $this->settings = \yii\helpers\Json::encode($this->settingsArr);
        return parent::beforeSave($insert);
    }
    
    public static function getRolesAccess($moduleId, $controllerId)
    {
        $rules = [];
        $roles = RolesSettings::find()->all();
        foreach ($roles as $role) {
            if (!isset($role->settingsArr[$moduleId])) { continue; }
            foreach ($role->settingsArr[$moduleId] as $block) {
                
                foreach ($block['controllers'] as $key => $controller) {
                    if ($key != $controllerId) { continue; }
                    foreach ($controller as $key => $controllerItem) {
                        if ($controllerItem['allowed'] != '0') {
                            $rules[] = [
                                'actions' => $controllerItem['items'],
                                'allow' => true,
                                'roles' => [$role->id],
                            ];
                        }
                    }
                }
            }
        }
        return [
            'class' => \yii\filters\AccessControl::className(),
            'rules' => $rules,
        ];
    }

    public static function getAccessRules($mKey)
    {
        $roles = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());
        $rules = [];
        foreach ($roles as $role) {
            $model = RolesSettings::findOne($role->name);
            if (!$model) {
                return false;
            }
            foreach ($model->settingsArr as $moduleKey => $module) {
                if ($moduleKey != $mKey) {
                    continue;
                }
                foreach ($module as $blockKey => $block) {
                    foreach ($block['controllers'] as $controllerKey => $controller) {
                        foreach ($controller as $controllerItemKey => $controllerItem) {
                            $allowed = $controllerItem['allowed'] == '1' ? true : false;
                            foreach ($controllerItem['items'] as $action) {
                                $rules[$controllerKey.'/'.$action] = $allowed;
                            }
                        }
                    }
                }
            }
            break;
        }
        return $rules;
    }

    public function loadBasicSettings() 
    {
        return [
            'shop' => [
                [
                    'block_name' => 'Категории',
                    'controllers' => [
                        'category' => [
                            [
                                'allowed' => false, // true|false
                                'name' => 'Просмотр',
                                'items' => [
                                    'index',
                                    'view',
                                ],
                            ],
                            [
                                'allowed' => false, // true|false
                                'name' => 'Редактирование',
                                'items' => [
                                    'create',
                                    'update',
                                    'delete',
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'block_name' => 'Товары',
                    'controllers' => [
                        'product' => [
                            [
                                'allowed' => false, // true|false
                                'name' => 'Просмотр',
                                'items' => [
                                    'index',
                                    'view',
                                ],
                            ],
                            [
                                'allowed' => false, // true|false
                                'name' => 'Редактирование',
                                'items' => [
                                    'create',
                                    'update',
                                    'delete',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'app' => [
                [
                    'block_name' => 'Управление seo',
                    'controllers' => [
                        'seo-items' => [
                            [
                                'allowed' => false, // true|false
                                'name' => 'Просмотр',
                                'items' => [
                                    'index',
                                    'view',
                                ],
                            ],
                            [
                                'allowed' => false, // true|false
                                'name' => 'Редактирование',
                                'items' => [
                                    'create',
                                    'update',
                                    'delete',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
