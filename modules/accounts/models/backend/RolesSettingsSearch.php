<?php

namespace app\modules\accounts\models\backend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\accounts\models\backend\RolesSettings;

/**
 * RolesSettingsSearch represents the model behind the search form about `app\modules\accounts\models\RolesSettingsSearch`.
 */
class RolesSettingsSearch extends RolesSettings
{
    
    public function __construct() {
        return true;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_role', 'settings'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RolesSettings::find()->where(['!=', 'user_role', 'admin'])
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'user_role', $this->user_role])
            ->andFilterWhere(['like', 'settings', $this->settings]);

        return $dataProvider;
    }
}
