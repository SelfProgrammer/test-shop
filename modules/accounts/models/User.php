<?php

namespace app\modules\accounts\models;


use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\modules\accounts\Module;
use Intervention\Image\ImageManagerStatic as Image;


/*
 * This is the model class for collection "accounts_user".
 *
 * @property $id
 * @property string $auth_key
 * @property string $access_token
 * @property integer $created_at
 * @property integer $updated_at
 * 
 * @property string $email
 * @property string $password_hash
 * @property string $name
 * @property string $avatar
 * @property string $position
 * @property integer $card_id
 * @property integer $status
 * @property integer $is_admin

 *
 * @property UploadedFile $avatar_upload
 * @property null|string $password
 * @property AccountsCards $card
 * @property AccountsUserData $userDatas
 * @property AccountsUserDevices[] $accountsUserDevices
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * string|null Расширение для файла(изображения) аватара
     */
    const AVATAR_FILE_EXTENSION = 'jpg';

    /**
     * integer Ширина изображения
     */
    const AVATAR_IMAGE_WIDTH  = 100;

    /**
     * integer Высота изображения
     */
    const AVATAR_IMAGE_HEIGHT = 100;

    /**
     * integer Качество изображения при сохранении
     */
    const AVATAR_IMAGE_QUALITY  = 75;

    /**
     * string Сценарий для добавления пользователя
     */
    const SCENARIO_CREATE = 'create';

    /**
     * string Сценарий для обновления информации существующего пользователя
     */
    const SCENARIO_UPDATE = 'update';
    
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TEST = 'test';

    /**
     * int Минимальное кол-во символов в пароле
     */
    const PASSWORD_MIN_LENGTH = 6;

    /**
     * @var string Виртуальный атрибут для назначения пароля
     */
    public $password;

    /**
     * @var UploadedFile Виртуальный атрибут используется для загрузки файла(avatar)
     */
    public $avatar_upload;

    /**
     * @var boolean Удаление заруженной аватарки
     */
    public $avatar_delete;


    const IS_ADMIN = 1;
    const NO_ADMIN = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAIT = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accounts_user';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            //systyem attributes
            'id',
            'auth_key',
            'access_token',
            'created_at',
            'updated_at',
            // other attributes
            'email',
            'password_hash',
            'name',
            'avatar',
            'position',
        ];
    }
    
    public function afterSave($insert, $changedAttributes) 
    {
        Yii::$app->authManager->revokeAll($this->id);
        if ($this->position) {
            $userRole = Yii::$app->authManager->getRole($this->position);
            if ($userRole) {
                Yii::$app->authManager->assign($userRole, $this->id);
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['email', 'name', 'position', 'password', 'avatar_upload'],
            self::SCENARIO_UPDATE => ['email', 'name', 'position', 'password', 'avatar_upload', 'avatar_delete'],
            self::SCENARIO_DEFAULT => ['email', 'name', 'position', 'password', 'avatar_upload', 'avatar_delete'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // default rules
            [['email', 'password_hash', 'name', 'avatar', 'position', 'password'], 'string'],
            [['created_at', 'updated_at'], 'integer'],

            [['position'], 'default', 'value' => null],
            // email rules
            [['email'], 'email'],
            [['email'], 'unique'],
            [['email'], 'default', 'value' => null],



            // custom rules
            [['name'], 'required'],
            [['password'], 'required', 'on' => [self::SCENARIO_CREATE]],
            [['password'], 'string', 'min' => self::PASSWORD_MIN_LENGTH],


            // for crop
            [
                ['avatar_upload'],
                'image',
                'skipOnEmpty' => true,
                'extensions'  => self::allowImageExtensions(),
                'mimeTypes'   => self::allowImageMimeTypes()
            ],
            [['avatar_delete'], 'boolean'],
        ];
    }

    public static function allowImageExtensions()
    {
        return ['jpg', 'jpeg', 'png', 'gif'];
    }
    public static function allowImageMimeTypes()
    {
        return ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'email'         => 'E-mail',
            'password_hash' => 'Password Hash',
            'name'          => 'Имя',
            'avatar'        => 'Аватарка',
            'position'      => 'Роль доступа',
            'created_at'    => 'Добавлен',
            'updated_at'    => 'Последнее изменение',

            // virtual attributes
            'password'      => 'Пароль',
            'avatar_upload' => 'Загрузить аватарку',
            'avatar_delete' => 'Удалить загруженную аватарку',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
    
    /**
     * @param string $id
     * @return self|static
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @param string $token
     * @param mixed $type
     * @return null|self
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $userDevice = UserDevices::findOne(['access_token' => $token]);
        if ($userDevice && isset($userDevice->user)) {
            return $userDevice->user;
        }
        return null;
    }

    public static function findByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * @return \MongoDB\BSON\ObjectID|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }
    
    
    /**
     * Установка(измение) пароля
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function beforeSave($insert)
    {
        if (!empty($this->password)) {
            $this->setPassword($this->password);
        }

        if ($this->avatar_delete) {
            $this->deleteAvatar();
        }

        $this->uploadAvatar();

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function deleteAvatar()
    {
        if (!empty($this->avatar)) {
            $module = Module::getInstance();
            $files = FileHelper::findFiles($module->getUploadPath(),[
                'only' => [
                    $this->avatar,
                ],
            ]);
            foreach ($files as $file) {
                @unlink($file);
            }
            $this->avatar = null;
        }
        return true;
    }

    public function uploadAvatar()
    {
        if (($this->avatar_upload instanceof UploadedFile) === false) {
            return true; // если изображение обязательное к загрузке то возможно стоит поставить false
        }
        if ($this->validate(['avatar_upload']) === false) {
            return false;
        }

        $module = Module::getInstance();
        $dir = $module->getUploadPath(true);
        $fileNamePrefix = $module->getUploadFilePrefix();
        $fileName = uniqid($fileNamePrefix);
        $fileExtension = !empty(self::AVATAR_FILE_EXTENSION) ? self::AVATAR_FILE_EXTENSION : $this->avatar_upload->extension;
        $fileNameWithExtension = $fileName. '.' . $fileExtension;
        $fullPath = $dir . $fileNameWithExtension;

        $saveResult = Image::make($this->avatar_upload->tempName)
            ->fit(self::AVATAR_IMAGE_WIDTH, self::AVATAR_IMAGE_HEIGHT)
            ->save($fullPath,self::AVATAR_IMAGE_QUALITY);

        if ($saveResult) {
            $this->deleteAvatar();
            $this->avatar = $fileNameWithExtension;
            return true;
        } else {
            return false;
        }
    }
    
    public function getAvatar()
    {
        if (!empty($this->avatar)) {
            $module = Module::getInstance();
            return $module->getUploadUrl(true) . $this->avatar;
        }
        return null;
    }
}
