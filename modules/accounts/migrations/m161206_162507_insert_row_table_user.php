<?php

use yii\db\Migration;

class m161206_162507_insert_row_table_user extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%accounts_user}}', array(
                'email' => 'admin@mail.com',
                'password_hash' => '$2y$13$MUsQPM8BlxTlfzqetP4oiO1ufVFqW0/gjsI8Jn8ZIgtYLOiQdyeU2',
                'name' => 'Admin',
        ));
    }

    public function safeDown()
    {
        echo "m160326_111728_insert_row_table_user cannot be reverted.\n";
        return true;
    }
    
}
