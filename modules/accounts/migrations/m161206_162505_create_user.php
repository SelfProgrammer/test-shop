<?php

use yii\db\Migration;

class m161206_162505_create_user extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        $this->createTable('{{%accounts_user}}', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(),
            'access_token' => $this->string(),
            'email' => $this->string(),
            'password_hash' => $this->string(),
            'name' => $this->string(),
            'avatar' => $this->string(),
            'position' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%accounts_user}}');
    }
}
