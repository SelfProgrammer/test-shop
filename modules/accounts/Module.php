<?php

namespace app\modules\accounts;

use Yii;
use yii\base\InvalidConfigException;

/*
 * @property string $controllerNamespace
 * @property integer $sessionDuration
 * @property string $uploadPath
 * @property string $uploadUrl
 * @property string $uploadFilePrefix
 * 
 * accounts module definition class
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\accounts\controllers';

    /**
     * Время жизни сессия после авторизации, если юзер используется галочку "запомнить меня"
     * @var int
     */
    public $sessionDuration = 2592000; // 3600*24*30
    
    /**
     * @var string Путь для загрузки изображений в системе, можно указать либо абсолюный путь системы, либо yii alias
     */
    public $uploadPath = '@webroot/uploads/accounts';

    /**
     * @var string Url куда обращаться для http запроса изображений, можно использовать как абслютный(например с http://cdn.domain.com/images) так и yii alias
     */
    public $uploadUrl = '@web/uploads/accounts';

    /**
     * @var string Префикс для названия файла изобржения, используется для уникализации имени если изображения лежат в общей папке с другими изображения.
     */
    public $uploadFilePrefix = 'account_';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // только для console
        if (Yii::$app instanceof \yii\console\Application) {
            $this->migrateCommandsInit();
            $this->customCommandsInit();
        }

        // только для web
        if (Yii::$app instanceof \yii\web\Application) {
            $this->adminInit();

            if (YII_DEBUG === true) {
                if (!is_dir($this->getUploadPath())) {
                    throw new InvalidConfigException('Каталог для загрузки изображений отсутствует!');
                }
            }

        }
        $this->urlManagerRulesInit();
    }
    
    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
    }

    /**
     * Делает так что бы при вызове yii в консоли были видны команды модуля в стиле <moduleName>/<commandName>/<commandAction>
     */
    public function customCommandsInit()
    {
        $this->controllerNamespace = 'app\modules\accounts\commands';
    }

    /**
     * Добавляет возможность использования миграций в рамках данного модуля
     */
    public function migrateCommandsInit()
    {
        $this->controllerMap['migrate'] = [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'accounts_migration',
            'migrationPath' => '@app/modules/accounts/migrations',
        ];
    }


    /**
     * Различные настройки для админки, например пункты меню и controllerMap
     * @return bool
     */
    public function adminInit()
    {
        /* @var $adminModule \app\modules\admin\Module */
        $adminModule = Yii::$app->getModule('admin');
        if (!$adminModule) {
            return false;
        }

        $adminModule->addControllerMap('user', [
            'class' => 'app\modules\accounts\controllers\backend\UserController',
            'viewPath' => '@app/modules/accounts/views/backend/user',
        ]);
 
        $adminModule->addControllerMap('roles', [
            'class' => 'app\modules\accounts\controllers\backend\RolesController',
            'viewPath' => '@app/modules/accounts/views/backend/roles',
        ]);

        $roles = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());
        foreach ($roles as $role) {
            if ($role->description == 'admin') {
                $adminModule->addLeftMenuItem([
                    'label' => 'Администрация',
                    'url' => '#administration',
                    'icon' => 'eye',
                    'controllerId' => ['user', 'roles'],
                    'sort' => 990,
                    'items' => [
                        [
                            'label' => 'Аккаунты',
                            'url' => ['/admin/user/index'],
                            'controllerId' => ['user'],
                        ],
                        [
                            'label' => 'Роли доступа',
                            'url' => ['/admin/roles/index'],
                            'controllerId' => 'roles',
                        ],
                    ],
                ]);
                //добавим пукнт меню в админку
                break;
            }
        }
    }

    /**
     * Различные настройки для сайта, например пункты в главном меню
     * @return bool
     */
    public function appInit()
    {
        return true;
    }

    /**
     * Добавляет роуты
     */
    public function urlManagerRulesInit()
    {
        $rules = [
            //'/admin'
        ];
        Yii::$app->urlManager->addRules($rules);
    }

    /**
     * @param bool $trailingDirectorySeparator Добавить в конец системны разделитель каталогов
     * @return bool|string
     */
    public function getUploadPath($trailingDirectorySeparator = false)
    {
        if (!($path = Yii::getAlias($this->uploadPath))) {
            throw new InvalidConfigException('qweqwe');
        }
        return $path . ($trailingDirectorySeparator ? DIRECTORY_SEPARATOR : '');
    }

    /**
     * @return bool|string
     */
    /**
     * @param bool $trailingSlash Добавить слэш в конец
     * @return string
     */
    public function getUploadUrl($trailingSlash = false)
    {
        return Yii::getAlias($this->uploadUrl) . ($trailingSlash ? '/' : '');
    }

    /**
     * @return bool|string
     */
    public function getUploadFilePrefix()
    {
        return $this->uploadFilePrefix;
    }
}
