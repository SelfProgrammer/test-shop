<?php

namespace app\modules\accounts\commands;

use Yii;
use yii\helpers\Console;


class RbacController extends \yii\console\Controller
{
    /**
     * Create user roles
     * @return array list of new migrations
     */
    public function actionIndex()
    {
        $role = Yii::$app->authManager->createRole('root');
        $role->description = 'admin';
        Yii::$app->authManager->add($role);
        $this->printCreateRole('root');
    }

    public function actionSetRole()
    {
        $userRole = Yii::$app->authManager->getRole('root');
        Yii::$app->authManager->assign($userRole, 1);
        echo 'Set role : ' . $userRole->description . ' for user ' . "\n";

    }

    public function actionGetRoles()
    {
        print_r(Yii::$app->authManager->getRolesByUser('57699d6aea8e2c06ec0051e1'));
    }

    protected function printCreateRole($role)
    {
        $role = $this->ansiFormat($role, Console::FG_CYAN);
        echo $this->ansiFormat('Added role:', Console::FG_GREEN) . " $role\n";
    }


}
