<?php

namespace app\modules\app;

use Yii;

/**
 * app module definition class
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\app\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (Yii::$app instanceof \yii\console\Application) {
            $this->migrateCommandsInit();
            $this->customCommandsInit();
        }
        if (Yii::$app instanceof \yii\web\Application) {
            $this->adminInit();
        }
        $this->urlManagerRulesInit();
    }

    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
    }


    /**
     * Делает так что бы при вызове yii в консоли были видны команды в стиле <moduleName>/<commandName>/<commandAction>
     */
    public function customCommandsInit()
    {
        $this->controllerNamespace = 'app\modules\app\commands';
    }

    /**
     * Добавляет возможность использования миграций MongoDB в рамках данного модуля
     */
    public function migrateCommandsInit()
    {
        $this->controllerMap['migrate'] = [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'app_migration',
            'migrationPath' => '@app/modules/app/migrations',
        ];
    }

    public function adminInit()
    {
        /* @var $adminModule \app\modules\admin\Module */
        $adminModule = Yii::$app->getModule('admin');
        if (!$adminModule) {
            return false;
        }

        //добавим контроллер в админку
        $adminModule->addControllerMap('seo-items', [
            'class' => 'app\modules\app\controllers\backend\SeoController',
            'viewPath' => '@app/modules/app/views/backend/seo',
        ]);

        $accessRules = \app\modules\accounts\models\backend\RolesSettings::getAccessRules('app');
        if (!$accessRules) {
            return false;
        }

        if ($accessRules['seo-items/index']) {
            $adminModule->addLeftMenuItem([
                'label' => 'SEO',
                'url' => ['/admin/seo-items/index'],
                'icon' => 'dashboard',
                'controllerId' => ['seo-items'],
                'sort' => 960,
            ]);
        }

    }

    /**
     * Добавляет роуты
     */
    public function urlManagerRulesInit()
    {
        //use \yii\web\UrlRule;
        $rules = [
        ];

        Yii::$app->urlManager->addRules($rules);
    }

}
