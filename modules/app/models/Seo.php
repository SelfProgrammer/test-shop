<?php

namespace app\modules\app\models;

use Yii;

/**
 * This is the model class for table "seo".
 *
 * @property int $id
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_desc
 * @property string $meta_text
 * @property string $url
 */
class Seo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meta_title', 'meta_keywords', 'meta_desc', 'meta_text', 'url'], 'string'],
            ['url', 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_desc' => 'Meta Description',
            'meta_text' => 'Meta Text',
            'url' => 'Url',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SeoQuery(get_called_class());
    }
}
