<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\redactor\widgets\Redactor;

/* @var $this yii\web\View */
/* @var $model app\modules\app\models\Seo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'meta_title')->textInput() ?>

    <?= $form->field($model, 'meta_keywords')->textInput() ?>

    <?= $form->field($model, 'meta_desc')->textarea(['rows' => 6]); ?>

    <?= $form->field($model, 'meta_text')->widget(Redactor::className(), [
        'clientOptions' => [
            'plugins' => Yii::$app->params['editor_plugins_list_full'],
        ]
    ]) ?>

    <?= $form->field($model, 'url')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
