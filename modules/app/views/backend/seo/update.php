<?php

use yii\helpers\Html;
use app\modules\app\Module;

/* @var $this yii\web\View */
/* @var $model app\modules\app\models\Seo */

$this->title = 'Редактирование правила';
$this->params['breadcrumbs'][] = ['label' => 'Seo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
