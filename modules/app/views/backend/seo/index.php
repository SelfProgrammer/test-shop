<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\app\Module;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\app\models\SeoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seo';
$this->params['breadcrumbs'][] = $this->title;
$accessRules = app\modules\accounts\models\backend\RolesSettings::getAccessRules('app');

$template = '{view}';
if ($accessRules && $accessRules['seo-items/update']) {
    $template .= ' {update} {delete}';
}
?>
<div class="seo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить правило', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'meta_title:ntext',
            'url:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
            ],
        ],
    ]); ?>
</div>
