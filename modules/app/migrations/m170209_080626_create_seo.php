<?php

use yii\db\Migration;

class m170209_080626_create_seo extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        $this->createTable('{{%seo}}', [
            'id' => $this->primaryKey(),
            'meta_title' => $this->text(),
            'meta_keywords' => $this->text(),
            'meta_desc' => $this->text(),
            'meta_text' => $this->text(),
            'url' => $this->text(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%seo}}');
    }
}
