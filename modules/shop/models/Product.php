<?php

namespace app\modules\shop\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use app\modules\shop\Module;
use yii\web\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string $image
 * @property double $price
 * @property double $special_price
 * @property int $created_at
 * @property int $updated_at
 * @property string $meta_title 
 * @property string $meta_keywords 
 * @property string $meta_desc 
 * @property string $meta_text
 * 
 * @property ProductToCategory[] $productToCategories
 */
class Product extends \yii\db\ActiveRecord
{
    public $img_upload;
    
    public $to_category;
    
    const IMAGE_QUALITY  = 85;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['to_category'], 'safe'],
            [['description'], 'string'],
            [['meta_title', 'meta_keywords', 'meta_desc', 'meta_text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['price', 'special_price'], 'number'],
            [['name', 'slug', 'image'], 'string', 'max' => 255],
            [
                ['img_upload'],
                'image',
                'skipOnEmpty' => true,
                'extensions'  => self::allowImageExtensions(),
                'mimeTypes'   => self::allowImageMimeTypes()
            ],
        ];
    }

    public static function allowImageExtensions()
    {
        return ['jpg', 'jpeg', 'png', 'gif'];
    }
    public static function allowImageMimeTypes()
    {
        return ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'slug' => 'ЧПУ',
            'image' => 'Изображение',
            'price' => 'Цена',
            'special_price' => 'Акционная цена',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Последнее изменение',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_desc' => 'Meta Description',
            'meta_text' => 'Meta Text',
            'img_upload' => 'Загрузить изображение',
            'to_category' => 'Показывать в категориях',
        ];
    }

    /**
    * @return ProductToCategoryQuery the active query used by this AR class.
    */ 
    public function getProductToCategories() 
    { 
        return $this->hasMany(ProductToCategory::className(), ['product_id' => 'id']); 
    }
    
    /**
     * {@inheritdoc}
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'        => SluggableBehavior::className(), // для того что бы работало преобразование криллицы в латинское ЧПУ нужен extension=php_intl.dll
                'attribute'    => ['name'],
                'immutable'    => true, // не генерировать новый ЧПУ у уже созданной модели
                'ensureUnique' => true, // делать ЧПУ уникальным
            ],
        ];
    }
    
    public function uploadImg()
    {
//        var_dump($this->img_upload);
        if (($this->img_upload instanceof UploadedFile) === false) {
            return true; // если изображение обязательное к загрузке то возможно стоит поставить false
        }
        if ($this->validate(['img_upload']) === false) {
            return false;
        }

        $module = Module::getInstance();
        $dir = $module->getUploadPath(true);
        $fileNamePrefix = $module->getUploadFilePrefix('product');
        $fileName = uniqid($fileNamePrefix);
        $fileExtension = $this->img_upload->extension;
        $fileNameWithExtension = $fileName. '.' . $fileExtension;
        $fullPath = $dir . $fileNameWithExtension;

        $saveResult = Image::make($this->img_upload->tempName)
//            ->fit(self::AVATAR_IMAGE_WIDTH, self::AVATAR_IMAGE_HEIGHT)
            ->save($fullPath,self::IMAGE_QUALITY);

        if ($saveResult) {
            $this->image = $fileNameWithExtension;
            return true;
        } else {
            return false;
        }
    }
    
    public function getImage($cacheSizeName = false)
    {
        if (empty($this->image)) {
            return \app\components\Helper::getNoImaeUrl();
        }
        
        $module = Module::getInstance();
        if ($cacheSizeName && isset(Yii::$app->params['image_cashe_sizes'][$cacheSizeName])) {
            $sizes = Yii::$app->params['image_cashe_sizes'][$cacheSizeName];
            $fileName = $sizes[0] . 'x' . $sizes[1] . '_' . $this->image;
            $file = $module->getUploadPath(true) . $fileName;
//            var_dump($file); die();
            if (!file_exists($file)) {
                $saveResult = Image::make($module->getUploadPath(true) . $this->image)
                ->resize($sizes[0], $sizes[1], function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
                ->save($file,self::IMAGE_QUALITY);
                if (!$saveResult) {
                    return \app\components\Helper::getNoImaeUrl();
                } else {
                    return $module->getUploadUrl(true) . $fileName;
                }
            } else {
                return $module->getUploadUrl(true) . $fileName;
                
            }
            
        }
        return $module->getUploadUrl(true) . $this->image;
    }
    
    public function beforeSave($insert)
    {
        $this->uploadImg();
        
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    
    public function afterSave($insert, $changedAttributes) 
    {
        if ($this->to_category && is_array($this->to_category)) {
            foreach ($this->productToCategories as $toCategory) {
                $toCategory->delete();
            }
            foreach ($this->to_category as $to_category) {
                $md = new ProductToCategory;
                $md->product_id = $this->id;
                $md->category_id = $to_category;
                $md->save(false);
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function afterFind() {
        foreach ($this->productToCategories as $toCategory) {
            $this->to_category[] = $toCategory->category_id;
        }
        return parent::afterFind();
    }
    
    public function getBreadcrumbs($referrer = false, $withLastLink = false) 
    {
        if ($withLastLink) {
            $items = [['label' => $this->name, 'url' => ['/shop/product/view', 'slug' => $this->slug, 'id' => $this->id]]];
        } else {
            $items = [$this->name];
        }
        
        if ($referrer) {
            $items = $this->getReferrerBreadcrumbs($referrer, $items);
        }
        
        return array_reverse($items);
    }
    
    public function getReferrerBreadcrumbs($referrer, $items) 
    {
        $request = new \yii\web\Request(['url' => parse_url($referrer, PHP_URL_PATH)]);
        $url = \Yii::$app->urlManager->parseRequest($request);
        if (!isset($url[0]) || $url[0] !== 'shop/category/view') {
            return $items;
        }
        if (!isset($url[1]) || !isset($url[1]['slug']) || !isset($url[1]['id'])) {
            return $items;
        }
        
        $category = Category::find()->where([
            'slug' => $url[1]['slug'],
            'id' => (int)$url[1]['id'],
        ])->one();
        
        if (!$category) {
            return $items;
        }
        
        $items[] = ['label' => $category->name, 'url' => ['/shop/category/view', 'slug' => $category->slug, 'id' => $category->id]];
        while (($category = $category->parent)) {
            $items[] = ['label' => $category->name, 'url' => ['/shop/category/view', 'slug' => $category->slug, 'id' => $category->id]];
        }
        
        return $items;
    }
}
