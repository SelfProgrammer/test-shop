<?php

namespace app\modules\shop\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $parent_id
 * @property int $created_at
 * @property int $updated_at
 * @property string $meta_title 
 * @property string $meta_keywords 
 * @property string $meta_desc 
 * @property string $meta_text
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property ProductToCategory[] $productToCategories 
 */
class Category extends \yii\db\ActiveRecord
{
    public $categoriesList = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'required'],
            [['meta_title', 'meta_keywords', 'meta_desc', 'meta_text'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'ЧПУ',
            'parent_id' => 'Родительская категория',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Последнее изменение',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_desc' => 'Meta Description',
            'meta_text' => 'Meta Text',
        ];
    }

    /**
    * @return ProductToCategoryQuery the active query used by this AR class.
    */ 
    public function getProductToCategories() 
    { 
        return $this->hasMany(ProductToCategory::className(), ['category_id' => 'id']); 
    } 
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'        => SluggableBehavior::className(), // для того что бы работало преобразование криллицы в латинское ЧПУ нужен extension=php_intl.dll
                'attribute'    => ['name'],
                'immutable'    => true, // не генерировать новый ЧПУ у уже созданной модели
                'ensureUnique' => true, // делать ЧПУ уникальным
            ],
        ];
    }
    
    public static function getCategoriesSelectList()
    {
        $model = new Category;
        $list = $model->getCategoriesAdminList();
        return $list;
    }
    
    public function getCategoriesAdminList()
    {
        $query = self::find()->where(['is', 'parent_id', null])->orderBy(['name' => SORT_ASC]);
        $delimetr = '-- ';
        
        foreach ($query->all() as $rootCategory) {
            $this->getWithChilds($rootCategory, $delimetr);
        }
        
        return $this->categoriesList;
    }
    
    private function getWithChilds($category, $delimetr) 
    {
        $outArr = [];
        $this->categoriesList[$category->id] = $delimetr.$category->name;
        if (count($category->categories) > 0) {
            foreach ($category->categories as $subCategory) {
                $this->getWithChilds($subCategory, ' -- '.$delimetr);
            }
        }
    }
    
    public static function getFrontTopMenu()
    {
        $items = [];
        $query = self::find()->where(['is', 'parent_id', null])->orderBy(['name' => SORT_ASC]);
        foreach ($query->all() as $category) {
            $items[] = ['label' => $category->name, 'url' => ['/shop/category/view', 'slug' => $category->slug, 'id' => $category->id]];
        }
        return $items;
    }
    
    public function getBreadcrumbs($withLastLink = false)
    {
        if ($withLastLink) {
            $items = [['label' => $this->name, 'url' => ['/shop/category/view', 'slug' => $this->slug, 'id' => $this->id]]];
        } else {
            $items = [$this->name];
        }
        
        $model = $this;
        while (($model = $model->parent)) {
            $items[] = ['label' => $model->name, 'url' => ['/shop/category/view', 'slug' => $model->slug, 'id' => $model->id]];
        }
        
        return array_reverse($items);
    }
}
