<?php

namespace app\modules\shop;

use Yii;
use yii\base\InvalidConfigException;

/**
 * shop module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\shop\controllers';
    
    /**
     * @var string Путь для загрузки изображений в системе, можно указать либо абсолюный путь системы, либо yii alias
     */
    public $uploadPath = '@webroot/uploads/shop';

    /**
     * @var string Url куда обращаться для http запроса изображений, можно использовать как абслютный(например с http://cdn.domain.com/images) так и yii alias
     */
    public $uploadUrl = '@web/uploads/shop';

    /**
     * @var string Префикс для названия файла изобржения, используется для уникализации имени если изображения лежат в общей папке с другими изображения.
     */
    public $uploadCategoryPrefix = 'category_';
    public $uploadProductPrefix = 'product_';
    
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (Yii::$app instanceof \yii\console\Application) {
            $this->migrateCommandsInit();
            $this->customCommandsInit();
        }

        // только для web
        if (Yii::$app instanceof \yii\web\Application) {
            $this->adminInit();

            if (YII_DEBUG === true) {
                if (!is_dir($this->getUploadPath())) {
                    throw new InvalidConfigException('Каталог для загрузки изображений отсутствует!');
                }
            }

        }
        $this->urlManagerRulesInit();
    }
    
    public function bootstrap($app)
    {
        // TODO: Implement bootstrap() method.
    }
    
    /**
     * Делает так что бы при вызове yii в консоли были видны команды модуля в стиле <moduleName>/<commandName>/<commandAction>
     */
    public function customCommandsInit()
    {
        $this->controllerNamespace = 'app\modules\shop\commands';
    }

    /**
     * Добавляет возможность использования миграций в рамках данного модуля
     */
    public function migrateCommandsInit()
    {
        $this->controllerMap['migrate'] = [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'shop_migration',
            'migrationPath' => '@app/modules/shop/migrations',
        ];
    }
    
    /**
     * Различные настройки для админки, например пункты меню и controllerMap
     * @return bool
     */
    public function adminInit()
    {
        /* @var $adminModule \app\modules\admin\Module */
        $adminModule = Yii::$app->getModule('admin');
        if (!$adminModule) {
            return false;
        }
        
        $adminModule->addControllerMap('category', [
            'class' => 'app\modules\shop\controllers\backend\CategoryController',
            'viewPath' => '@app/modules/shop/views/backend/category',
        ]);
        $adminModule->addControllerMap('product', [
            'class' => 'app\modules\shop\controllers\backend\ProductController',
            'viewPath' => '@app/modules/shop/views/backend/product',
        ]);
        
        $accessRules = \app\modules\accounts\models\backend\RolesSettings::getAccessRules('shop');
        if (!$accessRules) {
            return false;
        }
        
        if ($accessRules['category/index']) {
            $adminModule->addLeftMenuItem([
                'label' => 'Категории',
                'url' => ['/admin/category/index'],
                'controllerId' => ['category'],
                'icon' => 'newspaper-o',
                'sort' => 870,
            ]);
        }
        if ($accessRules['product/index']) {
            $adminModule->addLeftMenuItem([
                'label' => 'Товары',
                'url' => ['/admin/product/index'],
                'controllerId' => ['product'],
                'icon' => 'newspaper-o',
                'sort' => 870,
            ]);
        }
    }
    
    /**
     * Добавляет роуты
     */
    public function urlManagerRulesInit()
    {
        $rules = [];
        $rules['/<slug:[a-zA-Z0-9_\-]+>/c<id:[0-9]+>'] = 'shop/category/view';
        $rules['/<slug:[a-zA-Z0-9_\-]+>/p<id:[0-9]+>'] = 'shop/product/view';
        $rules['/search'] = 'shop/product/search';
        Yii::$app->urlManager->addRules($rules);
    }
    
    /**
     * @param bool $trailingDirectorySeparator Добавить в конец системны разделитель каталогов
     * @return bool|string
     */
    public function getUploadPath($trailingDirectorySeparator = false)
    {
        if (!($path = Yii::getAlias($this->uploadPath))) {
            throw new InvalidConfigException('Отсутствует каталог изображений!');
        }
        return $path . ($trailingDirectorySeparator ? DIRECTORY_SEPARATOR : '');
    }

    /**
     * @return bool|string
     */
    /**
     * @param bool $trailingSlash Добавить слэш в конец
     * @return string
     */
    public function getUploadUrl($trailingSlash = false)
    {
        return Yii::getAlias($this->uploadUrl) . ($trailingSlash ? '/' : '');
    }

    /**
     * @return bool|string
     */
    public function getUploadFilePrefix($type = 'category')
    {
        switch ($type) {
            case 'category':
                return $this->uploadCategoryPrefix;
            case 'product':
                return $this->uploadProductPrefix;
            default:
                return $this->uploadCategoryPrefix;
        }
        
    }
}
