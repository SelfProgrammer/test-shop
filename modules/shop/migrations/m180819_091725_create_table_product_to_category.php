<?php

use yii\db\Migration;

/**
 * Class m180819_091725_create_table_product_to_category
 */
class m180819_091725_create_table_product_to_category extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        $this->createTable('{{%product_to_category}}', [
            'id' => $this->primaryKey(),
            
            'product_id' => $this->integer(),
            'category_id' => $this->integer(),
            
        ], $tableOptions);
        
        $this->addForeignKey('fk__product_to_category__product_id__product__id', '{{%product_to_category}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('fk__product_to_category__category_id__category__id', '{{%product_to_category}}', 'category_id', '{{%category}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%product_to_category}}');
    }
}
