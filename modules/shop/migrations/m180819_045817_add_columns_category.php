<?php

use yii\db\Migration;

/**
 * Class m180819_045817_add_columns_category
 */
class m180819_045817_add_columns_category extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}','meta_title', $this->text());
        $this->addColumn('{{%category}}','meta_keywords', $this->text());
        $this->addColumn('{{%category}}','meta_desc', $this->text());
        $this->addColumn('{{%category}}','meta_text', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}','meta_title');
        $this->dropColumn('{{%category}}','meta_keywords');
        $this->dropColumn('{{%category}}','meta_desc');
        $this->dropColumn('{{%category}}','meta_text');
    }
}
