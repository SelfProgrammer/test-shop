<?php

use yii\db\Migration;

/**
 * Class m180819_062813_create_table_product
 */
class m180819_062813_create_table_product extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'slug' => $this->string(),
            
            'image' => $this->string(),
            'price' => $this->float([8,2]),
            'special_price' => $this->float([8,2]),
            
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            
            'meta_title' => $this->text(),
            'meta_keywords' => $this->text(),
            'meta_desc' => $this->text(),
            'meta_text' => $this->text(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
