<?php

use yii\widgets\ListView;

Yii::$app->controller->layout = '@app/views/layouts/with-sidebar.php';
$this->params['sidebarContent'] = $this->render('view/_sidebar-content', [
    'model' => $model,
    'categories' => $categories,
]);

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'view/_product_item',
    'summary' => '',
]);
?>


