<?php
    use yii\helpers\Url;
//    var_dump($categories);
?>
<ul class="sidebar-menu">
    <?php foreach ($categories as $category) : ?>
        <li><a href="<?= Url::to(['/shop/category/view', 'slug' => $category->slug, 'id' => $category->id])?>"><?= $category->name?></a></li>
        <?php if ($category->id === $model->id && count($category->categories) > 0) : ?>
            <?php foreach ($category->categories as $subCategory) : ?>
                <li class="sub-category">
                    <a href="<?= Url::to(['/shop/category/view', 'slug' => $subCategory->slug, 'id' => $subCategory->id])?>"><?= $subCategory->name?></a>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>