<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;

$accessRules = app\modules\accounts\models\backend\RolesSettings::getAccessRules('shop');
$template = '{view}';
if ($accessRules && $accessRules['product/update']) {
    $template .= ' {update} {delete}';
}
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'image',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\modules\accounts\models\User $model */
                    return Html::img($model->getImage('mini'));
                },
            ],
            'name',
//            'description:ntext',
//            'slug',
//            'image',
            'price',
            'special_price',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
            ],
        ],
    ]); ?>
</div>
