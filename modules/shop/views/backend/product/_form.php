<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\redactor\widgets\Redactor;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype'=>'multipart/form-data'
        ],
    ]); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <p>Мета теги</p>
            <?= $form->field($model, 'meta_title')->textInput() ?>
            <?= $form->field($model, 'meta_keywords')->textInput() ?>
            <?= $form->field($model, 'meta_desc')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'meta_text')->widget(Redactor::className(), [
                'clientOptions' => [
                    'plugins' => Yii::$app->params['editor_plugins_list_full'],
                ]
            ])?>
        </div>
    </div>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(Redactor::className(), [
        'clientOptions' => [
            'plugins' => Yii::$app->params['editor_plugins_list_full'],
        ]
    ])?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img_upload')->fileInput() ?>
    <?php if (($image = $model->getImage('normal'))): ?>
        <div class="img-preview">
            <?= Html::img($image) ?>
        </div>
    <?php endif ?>

    <?= $form->field($model, 'price')->textInput([
        'type' => 'number',
        'step' => '0.01',
    ]) ?>

    <?= $form->field($model, 'special_price')->textInput([
        'type' => 'number',
        'step' => '0.01',
    ]) ?>
    
    <?php 
    echo $form->field($model, 'to_category')->widget(\kartik\select2\Select2::className(),[
        'theme' => \kartik\select2\Select2::THEME_BOOTSTRAP,
        'showToggleAll' => false,
        'options' => [
            'multiple' => true,
            'placeholder' => 'Выберите категории ...'
        ],
//        'initValueText' => $model->getSpecInitialServicesApi(),
        'data' => \app\modules\shop\models\Category::getCategoriesSelectList(),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerCss("
    .img-preview {
        max-width: 500px;
        max-height: 500px;
    }
    .img-preview img {
        max-width: 100%;
        max-height: 100%;
    }
")?>