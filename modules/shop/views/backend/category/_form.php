<?php

use app\modules\shop\models\Category;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\redactor\widgets\Redactor;

/* @var $this View */
/* @var $model Category */
/* @var $form ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <p>Мета теги</p>
            <?= $form->field($model, 'meta_title')->textInput() ?>
            <?= $form->field($model, 'meta_keywords')->textInput() ?>
            <?= $form->field($model, 'meta_desc')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'meta_text')->widget(Redactor::className(), [
                'clientOptions' => [
                    'plugins' => Yii::$app->params['editor_plugins_list_full'],
                ]
            ])?>
        </div>
    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($model->getCategoriesAdminList(), [
        'prompt' => '',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
