<?php

use yii\helpers\Html;
/* * *ext** */
use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\rpg\models\TreeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;

$accessRules = app\modules\accounts\models\backend\RolesSettings::getAccessRules('shop');

$template = '{view}';
if ($accessRules && $accessRules['category/update']) {
    $template .= ' {update} {delete}';
}
?>
<div class="tree-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 
    <?= TreeGrid::widget([
        'dataProvider' => $dataProvider,
        'keyColumnName' => 'id',
        'parentColumnName' => 'parent_id',
        'parentRootValue' => '', //first parentId value
        'pluginOptions' => [
            'initialState' => 'collapsed',
        ],
        'columns' => [
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $template,
            ]
        ]     
    ]); ?>
    
    <?= $this->registerCss("
        .treegrid-expander {
            margin-top: 6px;
            vertical-align: top;
        }
        .tree-index td {
            line-height: 28px !important;
        }
    ")?>
</div>
