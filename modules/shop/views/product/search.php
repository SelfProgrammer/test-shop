<?php

use yii\widgets\ListView;

?>

<h3>По вашему запросу "<?= $q?>" найдено <?= $dataProvider->totalCount?> товара(ов)</h3>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_product_item',
    'summary' => '',
]);
?>