<?php
    use yii\helpers\Html;
?>

<div class="col-md-6 prod-img-wrap-inner">
    <?= Html::img($model->getImage('normal'))?>
</div>
<div class="col-md-6">
    <h1><?= $model->name?></h1>
    <div class="prod-price prod-price-inner">
        <?php if ($model->special_price) :?>
            <span><?= $model->special_price ?> грн.</span>
        <?php endif;?>
        <span><?= $model->price ?> грн.</span>
    </div>
    <div class="prod-description">
        <?= $model->description ?>
    </div>
</div>

