<?php

namespace app\modules\shop\controllers;

use app\components\BaseController;
use app\components\Helper;
use app\modules\shop\models\Category;
use app\modules\shop\models\ProductSearch;
use Yii;
use yii\web\NotFoundHttpException;

class CategoryController extends BaseController
{   
    public function actionView($slug, $id)
    {
        $model = $this->findModel($id);
        
        $searchModel = new ProductSearch();
        $searchModel->categoryId = $id;
        $dataProvider = $searchModel->searchFront();
        Helper::registerTags($model);
        Yii::$app->view->params['breadcrumbs'] = $model->getBreadcrumbs();
        $categories = Category::find()->where(['parent_id' => $model->parent_id])->all();
        
        return $this->render('view', [
            'model' => $model,
            'categories' => $categories,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена');
    }
}
