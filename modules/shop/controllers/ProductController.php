<?php

namespace app\modules\shop\controllers;

use app\components\BaseController;
use app\components\Helper;
use app\modules\shop\models\Product;
use Yii;
use yii\web\NotFoundHttpException;

class ProductController extends BaseController
{   
    public function actionView($slug, $id)
    {
        $model = $this->findModel($id);
        Helper::registerTags($model);
//        var_dump(\Yii::$app->request->referrer);
        Yii::$app->view->params['breadcrumbs'] = $model->getBreadcrumbs(\Yii::$app->request->referrer);
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена');
    }
    
    public function actionSearch()
    {
        $model = new \app\modules\shop\models\ProductSearch;
        $get = \Yii::$app->request->get();
        if (isset($get['q']) && !empty($get['q'])) {
            $model->name = $get['q'];
        }
        
        $dataProvider = $model->search([]);
        
        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'q' => isset($get['q']) ? $get['q'] : '',
        ]);
    }
}
