<?php

namespace app\components;

use app\modules\app\models\Seo;
use app\components\Helper;
use Yii;
use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action) {

        $url = \Yii::$app->request->url;
        $model = Seo::findOne([
            'url' => $url,
        ]);
        if ($model) {
            Helper::registerTags($model);
        }
        return parent::beforeAction($action);
    }
}
