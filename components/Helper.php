<?php

namespace app\components;

use Yii;

class Helper
{
    public static function getNoImaeUrl()
    {
        return '/uploads/no-image.png';
    }
    
    public static function registerTags($model)
    {

        if (isset($model->meta_title) && !empty($model->meta_title)) {
            \Yii::$app->view->title = $model->meta_title;
        }

        if (isset($model->meta_keywords) && !empty($model->meta_keywords)) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $model->meta_keywords], 'keywords');
        }

        if (isset($model->meta_desc) && !empty($model->meta_desc)) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $model->meta_desc], 'description');
        }
        
        if (isset($model->meta_text) && !empty($model->meta_text)) {
            \Yii::$app->params['metaText'] = $model->meta_text;
        }
    }
	
//    public static function mb_ucfirst($string, $encoding = 'utf-8')
//    {
//        $string = mb_strtolower($string, $encoding);
//        $strlen = mb_strlen($string, $encoding);
//        $firstChar = mb_substr($string, 0, 1, $encoding);
//        $then = mb_substr($string, 1, $strlen - 1, $encoding);
//        return mb_strtoupper($firstChar, $encoding) . $then;
//    }
}