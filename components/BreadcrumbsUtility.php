<?php
 
namespace app\components;
 
use yii\helpers\Url;
use yii\helpers\Html;
 
class BreadcrumbsUtility
{
    public static function UseMicroData($links, $home = 2) 
    {
        foreach ($links as $key => &$link) {
            if(is_array($link)) {
                $link['template'] = BreadcrumbsUtility::getTemplate($link['label'], $link['url'], $key+$home);
            }
        }
 
        return $links;
    }

    public static function getHome($label, $url) {
 
        $home = [
            'label' => $label,
            'url'   => $url,
            'template' => BreadcrumbsUtility::getTemplate($label, $url, 1)
        ];
 
        return $home;
    }

    protected static function getTemplate($label, $url, $key) {
        return '<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">'.
                    Html::a('<span itemprop="name">'.$label.'</span>', Url::to($url), ['itemprop'=>'item'])
                . '
                    <meta itemprop="position" content="'.$key.'" />
                </li>';
    }
}